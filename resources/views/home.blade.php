@extends('layouts.app')

@section('content')

<div class="row" id="cont">
        <div class="col-2" id="side">
                <div class="container">
                        <div class="row justify-content-center">
                                <img src="./img/logo.png" style="width:5rem; height:5rem; margin-top: 2.5rem; margin-bottom: 1.5rem;"/>
                        </div>
                        <div class="row justify-content-center" style="margin-bottom: 3rem;">
                        	<form method="POST" action="{{ route('logout') }}">
                        		{{ csrf_field() }}
								<button class="btn btn-danger">Cerrar sesión <i class="fa fa-sign-out" aria-hidden="true"></i></button>
							</form>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">PAGINA/SECCIONES</button>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">AGREGAR TOURS</button>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">LOGIN/PASS</button>
                        </div>
                </div>
        </div> <!-- Columna de lado -->

        <div class="col-10" id="side2">
            <div class="container" style="background-color: #e3e6e9; margin-top: 1rem; height:95%; padding: 2rem 2rem 1rem 2rem;">

            	
	                <a href="home_edit">
		            	<div class="row justify-content-between" id="secciones-link" style="box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.47);
				                                                                            border-radius: 5px;
				                                                                            background: #f0f3f5;
				                                                                            padding: 0.7rem 0.2rem 0.2rem 0.2rem;">
				            <div class="col-6">
				                <h4 style="color: #0b4650;"><strong>HOME/EDIT</strong></h4>
				            </div>
				            <div class="col-6" style="text-align: right; padding-right: 3rem;">
				                <input style="color: gray; border: none; width: 17rem; background: none;" value="ÚLTIMA EDICIÓN 18-03-2018" class="field left" readonly="readonly" name="updated_at">
				            </div>
				        </div>
			        </a>

			    <a href="about_us">
			        <div class="row justify-content-between" id="secciones-link" style="box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.47);
			                                                                            border-radius: 5px;
			                                                                            background: #f0f3f5;
			                                                                            padding: 0.7rem 0.2rem 0.2rem 0.2rem;">
			            <div class="col-6">
			                <h4 style="color: #0b4650;"><strong>ABOUT US/EDIT</strong></h4>
			            </div>
			            <div class="col-6" style="text-align: right; padding-right: 3rem;">
			                <input style="color: gray; border: none; width: 17rem; background: none;" value="ÚLTIMA EDICIÓN 18-03-2018" class="field left" readonly="readonly" name="updated_at">
			            </div>
			        </div>
			    </a>  
			        <div class="row justify-content-between" id="secciones-link" style="box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.47);
			                                                                            border-radius: 5px;
			                                                                            background: #f0f3f5;
			                                                                            padding: 0.7rem 0.2rem 0.2rem 0.2rem;">
			        <div class="col-6">
			                <h4 style="color: #0b4650;"><strong>TOUR</strong></h4>
			            </div>
			            <div class="col-6" style="text-align: right; padding-right: 3rem;">
			                <input style="color: gray; border: none; width: 17rem; background: none;" value="ÚLTIMA EDICIÓN" class="field left" readonly="readonly" name="updated_at">
			            </div>
			        </div>

			        <div class="row justify-content-between" id="secciones-link" style="box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.47);
			                                                                            border-radius: 5px;
			                                                                            background: #f0f3f5;
			                                                                            padding: 0.7rem 0.2rem 0.2rem 0.2rem;">
			        <div class="col-6">
			                <h4 style="color: #0b4650;"><strong>LA OTRA VERGA</strong></h4>
			            </div>
			            <div class="col-6" style="text-align: right; padding-right: 3rem;">
			                <input style="color: gray; border: none; width: 17rem; background: none;" value="ÚLTIMA EDICIÓN" class="field left" readonly="readonly" name="updated_at">
			            </div>
			        </div>
	       </div> <!-- Contenedor de la parte de contenido -->
	    </div> <!-- row gigante -->


@endsection
