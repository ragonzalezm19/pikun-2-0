<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    <style type="text/css">
        html {
             height: 100%;
             width: 100%;
        }
        body {
            background-color: #f6fafa;
            height: 100%;
            width: 100%;
            margin-top: 0px;
            font-family: 'Inconsolata', monospace;
        }
        #cont {
            width: 100%;
            height: 100%;
            background-color: #0c5460;
            min-height:100%;
            height: auto!important;
            height:100%;
            
        }

        #side {
            background:  #0c5460;
            height: 100%;
        }

        #side2 {
            background: rgb(255, 255, 255);
        }

        #btn-cus {
            border-radius: 0.5rem;
        }

        .col-2 {
                padding-right: 1px;
        }

        img {
		    width: 305px;
		}

        #icon-ing {
            position: absolute; width: 1rem;
            top: 9px;
            right: 24px;
        }

        #icon-esp {
            width: 1rem;
            top: 9px;
            position: absolute;
            right: 12px;
        }
        </style>

</head>
<body>



    <div class="row" id="cont">
        <div class="col-2" id="side">
                <div class="container">
                        <div class="row justify-content-center">
                                <img src="./img/logo.png" style="width:5rem; height:5rem; margin-top: 2.5rem; margin-bottom: 1.5rem;"/>
                        </div>
                        <div class="row justify-content-center" style="margin-bottom: 3rem;">
                                <button type="button" class="btn btn-danger">CERRAR SESIÓN</button>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">PAGINA/SECCIONES</button>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">AGREGAR TOURS</button>
                        </div>
        
                        <div class="row justify-content-center">
                                <button type="button" ID="btn-cus" class="btn btn-primary"; style="width: 93%; margin-bottom: 0.3rem; padding: 10px;">LOGIN/PASS</button>
                        </div>
                    </div>
        </div>

        <div class="col-10" id="side2">
            <div class="container" style="background-color: #e3e6e9; margin-top: 1rem; height:95%; padding: 2rem 1rem 1rem 1rem;">
                <div class="container" style="background-color: #f0f3f5; height: 100%; border-radius: 5px 5px 0px 0px;">
                    <form method="POST" action="{{ route('about_update') }}">
                        {{ csrf_field() }} 
                        <input type="hidden" name="id_registro" value="2" /> <!-- Valor del registro -->
                            <!-- TITULO -->
                            <div class="row justify-content-between" style="box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.47);
                                                                                    border-radius: 5px;
                                                                                    background: #f0f3f5;
                                                                                    padding: 0.7rem 0.2rem 0.2rem 0.2rem;">
                                <div class="col-6">
                                    <h4 style="color: #0b4650;"><strong>ABOUT US/EDIT</strong></h4>
                                </div>
                                <div class="col-6" style="text-align: right; padding-right: 3rem;">
                                    <input style="color: gray; border: none; width: 17rem; background: none;" value="ÚLTIMA EDICIÓN {{ $sectionInfo->updated_at }}" class="field left" readonly="readonly" name="updated_at">
                                </div>
                            </div>

                    

                        <!-- BOTONES GUARDAR / CERRAR -->
                        <div class="row" style="    text-align: right;
                                                    margin-top: 1rem;
                                                    margin-bottom: 1rem;">
                            <div class="col-12">
                                <button href ="dashboard" type="button" class="btn" style="background: #065e6e; color: white; width: 7rem;">CERRAR</button>
                                <button type="SUBMIT" class="btn" style="background: #00a751; color: white; width: 7rem;">ACTUALIZAR</button>
                            </div>    
                        </div>

                        <!-- TITULOS 1-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem; position: relative;">
                                <input type="text" name="titu_1_ing" value="{{ $sectionInfo->titu_1_ing }}" style=" width: 100%;   padding: 0.3rem;   background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-ing.png" id="icon-ing">
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_1_esp" value="{{ $sectionInfo->titu_1_esp }}" style="width: 100%; padding: 0.3rem;  background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-esp.png" id="icon-esp">
                            </div>
                        </div>

                        <!-- DESCRIPCIONES 1-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_1_ing" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_1_ing }}</textarea>
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_1_esp" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_1_esp }}</textarea>
                            </div>
                        </div>


                         <!-- TITULOS 2-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_2_ing" value="{{ $sectionInfo->titu_2_ing }}" style=" width: 100%;   padding: 0.3rem;   background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-ing.png" id="icon-ing">
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_2_esp" value="{{ $sectionInfo->titu_2_esp }}" style="width: 100%; padding: 0.3rem;  background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-esp.png" id="icon-esp">
                            </div>
                        </div>

                        <!-- DESCRIPCIONES 2-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_2_ing" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_2_ing }}</textarea>
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_2_esp" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_2_esp }}</textarea>
                            </div>
                        </div>

                         <!-- TITULOS 3-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_3_ing" value="{{ $sectionInfo->titu_3_ing }}" style=" width: 100%;   padding: 0.3rem;   background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-ing.png" id="icon-ing">
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_3_esp" value="{{ $sectionInfo->titu_3_esp }}" style="width: 100%; padding: 0.3rem;  background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-esp.png" id="icon-esp">
                            </div>
                        </div>

                        <!-- DESCRIPCIONES 3-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_3_ing" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_3_ing }}</textarea>
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_3_esp" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_3_esp }}</textarea>
                            </div>
                        </div>

                         <!-- TITULOS 4-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_4_ing" value="{{ $sectionInfo->titu_4_ing }}" style=" width: 100%;   padding: 0.3rem;   background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-ing.png" id="icon-ing">
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <input type="text" name="titu_4_esp" value="{{ $sectionInfo->titu_4_esp }}" style="width: 100%; padding: 0.3rem;  background: #e3e6e9; border: solid 1px #8181825c;">
                                <img src="./img/icon-esp.png" id="icon-esp">
                            </div>
                        </div>

                        <!-- DESCRIPCIONES 4-->
                        <div class="row">
                            <div class="col-6" style="padding-left: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_4_ing" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_4_ing }}</textarea>
                            </div>
                            <div class="col-6" style="padding-right: 3px; margin-bottom: 1rem;">
                                <textarea name="desc_4_esp" rows="30" cols="80" style=" width: 100%; padding: 0.8rem;  height: 10rem; background: #e3e6e9; border: solid 1px #8181825c;">{{ $sectionInfo->desc_4_esp }}</textarea>
                            </div>
                        </div>



                        <hr/>
                        
                        <!-- CARGAR IMAGENES -->
                        <div class="row">
                            <div class="col-6">
                                <h4>IMÁGENES DESKTOP</h4>
                            </div>

                            <div class="col-6">
                                <h4>IMÁGENES MÓVIL</h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <input type="file" id="files" name="files[]" />
                                <br />
                                <div>
                                        <output id="list"></output>
                                </div>
                            </div>
                            <div class="col-6">
                                <input type="file" id="files" name="files[]" />
                                <br />
                                <div>
                                    <output id="list"></output>
                                </div>
                            </div>
                        </div>
                    </form>                
                </div>
            </div>
        </div>
</body>
</html>