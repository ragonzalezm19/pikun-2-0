<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">


    </head>
<body>

<div class="container" class="wrap">
	<div class="container" id="conte" >
		<!-- Formulario -->
		<form method="POST" action="login">
			{{ csrf_field() }} 

			<!-- Ahora creado este metodo nos dira que le metodo login no existe y ahora lo creamos en en controlador -->
			<!-- <input type="hidden" name="token" value="{{ csrf_token() }}"> -->
			<div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
				<input name="usuario" 
					   placeholder="USUARIO" 
					   id="input-form"
					   value="{{ old('usuario') }}"> 
				{!! $errors->first('usuario', '<span class="help-block mensajerror">:message</span>') !!}
			</div>


			<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
				<input type="password" 
					   name="password" 
					   placeholder="PASSWORD" 
					   id="input-form1" 
					   style="color:gray; background: #dee2e6; margin-top: -7px;">
				{!! $errors->first('password', '<span class="help-block mensajerror">:message</span>') !!}
			</div>

			<button type="submit" class="btn btn-primary" style="background-color: #007bff00; border-color: #007bff00; position: absolute;"></button>
		</form>

		<!-- Imagen -->
		<div class="container">
			<div class="row">
				<div class="col-12" style="text-align: center;">
					<img src="./img/logo.png" id="img">
					<!-- {!! bcrypt('hola'); !!} -->
				</div>
			</div>
		</div>

		<!-- Recuperar Contraseña -->
		<div class="container" id="forgot">
			<!-- <a href="" id="text-forgot">RECUPERA TU CONTRASEÑA</a> -->
			<a class="btn btn-link" id="text-forgot" href="{{ route('password.request') }}">
                                    {{ __('RECUPERA TU CONTRASEÑA') }}
        </a>
		</div>		
	</div>
</div>
   
</body>
</html>

