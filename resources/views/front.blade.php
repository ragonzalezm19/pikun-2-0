<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link rel="stylesheet" href=""/>
    <link rel="stylesheet" href=""/>
    <script src=""></script>
    <script src=""></script>
    <title>Pikun Mapu</title>
  </head>
  <body>
    <div class="container-fluid">
      <!-- Menu-->
      <div id="home" class="col-md-12 menu">
        <div class="row menu__row row-no-margin" style="position: relative;z-index: 10000000000;">
          <div class="col-xs-12 col-md-1 menu__logo"><img class="img-responsive" src="" alt=""/></div>
          <div class="col-md-7 no-mobile"></div>
          <div class="col-md-4 menu__opciones no-mobile">
            <div class="menu__opcion1 opcion"><a href="#home">Home</a><span class="guion">-</span></div>
            <div class="menu__opcion2 opcion"><a href="#about-us">About Us</a><span class="guion">-</span></div>
            <div class="menu__opcion3 opcion"><a href="./tours.php">Tours</a><span class="guion">-</span></div>
            <div class="menu__opcion4 opcion"><a href="#contacto">Contact</a></div>
          </div>
        </div>
        <div class="row row__linea-roja no-mobile">
          <div class="col-md-2 col-md-offset-10 linea-roja"></div>
        </div>
        <!-- Seccion 1-->
        <div class="col-md-12 seccion1">
          <div class="row row-no-margin">
            <div class="col-xs-9 col-md-10 titulo">
              <div class="titulo__titulo">
                <h1>PRIVATE<br/>EXCURSIONS IN<br/>SANTIAGO DE CHILE</h1>
              </div>
              <div class="linea-blanca"></div>
              <div class="titulo__descripcion no-mobile">
                <p>
                  The primary objective of Pikun Mapu is to provide innovative and unique
                  travel experiences for the best value showcasing the most spectacular
                  places, attractions and activities in Santiago de Chile.
                </p>
              </div>
            </div>
            <div class="col-xs-3 col-md-1 meteored-widget">
              <div id="cont_058e3c45c446f7b96bb80501a82aca7d">
                <script type="text/javascript" async="async" src="https://www.meteored.cl/wid_loader/058e3c45c446f7b96bb80501a82aca7d"></script>
              </div>
            </div>
            <div class="col-md-1 indicador no-mobile">
              <div class="indicador-contenedor">
                <a href="#home"><div class="circulo activo"></div></a>
                <a href="#about-us"><div class="circulo"></div></a>
                <a href="#tours"><div class="circulo"></div></a>
                <a href="#contacto"><div class="circulo"></div></a>
              </div>
            </div>
          </div>
          <div class="row mobile row-no-margin">
            <div class="col-xs-12 descripcion">
              <div class="descripcion__descripcion">
                <p>
                  The primary objective of Pikun Mapu is to provide innovative and unique
                  travel experiences for the best value showcasing the most spectacular
                  places, attractions and activities in Santiago de Chile.
                </p>
              </div>
            </div>
          </div>
          <div class="row no-mobile">
            <div class="col-md-1 tripadvisor"><img class="img-responsive" src="" alt=""/></div>
            <div class="col-md-9"></div>
            <div class="col-md-2 lenguaje">
              <p>Language<img class="img-responsive" src="" alt=""/></p>
            </div>
          </div>
        </div>
      </div>
      <!-- Seccion 1.5-->
      <div class="col-md-12 tours mobile">
        <div class="row">
          <div class="col-xs-12 col-md-7 tours__titulo">
            <h1></h1>
          </div>
          <div class="col-xs-12 col-md-1"><img class="img-responsive" src="" alt=""/></div>
          <div class="col-xs-0 col-md-4"></div>
        </div>
      </div>
      <!-- Tours Mobile-->
      <div class="col-md-12 tours__mobile mobile">
        <div class="row tour-azul">
          <div class="col-xs-12 tour__container">
            <div class="col-xs-12 tour__azul__titulo">
              <h2>ANDEAN SUMMIT IN A DAY</h2>
            </div>
            <div class="col-xs-12 tour__azul__sub-titulo">
              <h3>¿Quieres alcanzar esta cumbre andina en tan solo un dia?</h3>
            </div>
            <div class="col-xs-12 tour__azul__descripcion">
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero similique praesentium, 
                quasi ut modi veritatis eius consequatur id voluptas animi cumque aliquid perspiciatis 
                reiciendis porro odit tempore omnis odio dolorem?
              </p>
            </div>
            <div class="col-xs-4 tour__azul__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Dificultat</span><span class="descripcion">Alta</span></div>
            </div>
            <div class="col-xs-4 tour__azul__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Duración</span><span class="descripcion">2hr</span></div>
            </div>
            <div class="col-xs-4 tour__azul__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Precio</span><span class="descripcion">$79.000 CLP p/p</span></div>
            </div>
          </div>
          <div class="col-xs-12 tour__azul__info"><img class="img-responsive" src="" alt=""/>
            <h3>INFO</h3>
          </div>
        </div>
        <div class="row tour-rojo">
          <div class="col-xs-12 tour__container">
            <div class="col-xs-12 tour__rojo__titulo">
              <h2>ANDEAN SUMMIT IN A DAY</h2>
            </div>
            <div class="col-xs-12 tour__rojo__sub-titulo">
              <h3>¿Quieres alcanzar esta cumbre andina en tan solo un dia?</h3>
            </div>
            <div class="col-xs-12 tour__rojo__descripcion">
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero similique praesentium, 
                quasi ut modi veritatis eius consequatur id voluptas animi cumque aliquid perspiciatis 
                reiciendis porro odit tempore omnis odio dolorem?
              </p>
            </div>
            <div class="col-xs-4 tour__rojo__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Dificultat</span><span class="descripcion">Alta</span></div>
            </div>
            <div class="col-xs-4 tour__rojo__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Duración</span><span class="descripcion">2hr</span></div>
            </div>
            <div class="col-xs-4 tour__rojo__caracteristica"><img class="img-responsive" src="" alt=""/>
              <div class="info"><span class="titulo">Precio</span><span class="descripcion">$79.000 CLP p/p</span></div>
            </div>
          </div>
          <div class="col-xs-12 tour__rojo__info"><img class="img-responsive" src="" alt=""/>
            <h3>INFO</h3>
          </div>
        </div>
      </div>
      <!-- Seccion 2-->
      <div id="about-us" class="col-md-12 seccion2">
        <div class="row">
          <div class="col-md-3 seccion2__titulo">
            <h2>ABOUT <span class="rojo">US</span></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 seccion2__descripcion">
            <p>
              Pikun Mapu is a special interest tour operator, our specialty is the adventure tourism offering activities such as hiking, rock
              climbing, trekking, horseback riding and mountaineering .We’ve carefully crafted our trips with one goal in mind; to enable our
              guests to take the best experience from Chile operating our excursions with the highest quality standard.
              
            </p>
          </div>
        </div>
      </div>
      <!-- Seccion 3-->
      <div class="col-md-12 seccion3">
        <div class="row">
          <div class="col-md-3 seccion3__titulo">
            <h2>SMALL <span class="rojo">GROUPS</span></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 seccion3__descripcion">
            <p>
              When we say small-group tours, we mean it. We limit our tour group size so you can travel unobtrusively, enjoying the freedom 
              and flexibility
              
            </p>
          </div>
        </div>
      </div>
      <!-- Seccion 4-->
      <div class="col-md-12 seccion4">
        <div class="row">
          <div class="col-md-3 seccion4__titulo">
            <h2>OUR <span class="rojo">GUIDES</span></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 seccion4__descripcion">
            <p>
              As well as covering the most famous sights, we pride ourselves on also showing our clients authentic, less touristy sights with
              excellent local guides, whose wealth of knowledge and experience will ensure you have a day out to remember. Our guides have
              specific studies in their disciplines in addition to a vast experience like athletes.
              
            </p>
          </div>
        </div>
      </div>
      <!-- Seccion 5-->
      <div class="col-md-12 seccion5">
        <div class="row">
          <div class="col-md-3 seccion5__titulo">
            <h2>PICUNCHES</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 seccion5__descripcion">
            <p>
              What ́s in a name? Mapuche is a local culture that expanded from the seventh region to the transverse valleys in the center of the
              country. Pikun Mapu was the expression by which mapuche people from the south called the region where today Santiago city is
              located, so Pikun Mapu means “land of the north”.
              
              
            </p>
          </div>
        </div>
      </div>
    
  </body>
</html>