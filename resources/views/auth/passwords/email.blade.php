@extends('layouts.app')

@section('content')
<div class="container" id="second">
    <div class="row align-items-center" id="rowid"> 
        <div class="col-md-8 offset-md-4" id="rowid-child">
            <div class="card" id="card-email">
                <!-- <div class="card-header">{{ __('Resetear contraseña') }}</div> -->

                <div class="container" id="ventana-email">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}" id="formp">
                        @csrf

                        <div class="form-group row">
                            <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                            <div class="col-md-6">
                                <input id="email" placeholder="Ingrese el correo de tu cuenta" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-0">
                                <button type="submit" class="btn btn-primary" id="btn-send">
                                    {{ __('Reestablecer') }}
                                </button>
                            </div>
                        </div>

                        <!-- Imagen -->
                        <div class="container">
                            <div class="row">
                                <div class="col-12" style="text-align: center;">
                                    <img src="../img/logo.png" id="logo-login">
                                    <!-- {!! bcrypt('hola'); !!} -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
