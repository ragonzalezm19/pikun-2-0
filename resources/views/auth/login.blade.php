@extends('layouts.app')

@section('content')
<div class="container" class="wrap">
	<div class="container" id="conte" >
		<!-- Formulario -->
		<form method="POST" action="{{ route('login') }}">
			{{ csrf_field() }} 

			<!-- Ahora creado este metodo nos dira que le metodo login no existe y ahora lo creamos en en controlador -->
			<!-- <input type="hidden" name="token" value="{{ csrf_token() }}"> -->
			<div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
                <input  type="email" 
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-login" 
                        name="email" 
                        placeholder="USUARIO"
                        value="{{ old('email') }}" 
                        required autofocus>
				{!! $errors->first('email', '<span class="help-block mensajerror">:message</span>') !!}
            </div>
            
			<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
				<input type="password" 
					   name="password" 
                       placeholder="PASSWORD"
                       class="input-login-pass">
				{!! $errors->first('password', '<span class="help-block mensajerror">:message</span>') !!}
			</div>

			<button type="submit" class="btn btn-primary" style="background-color: #007bff00; border-color: #007bff00; position: absolute;"></button>
		</form>

		<!-- Imagen -->
		<div class="container">
			<div class="row">
				<div class="col-12" style="text-align: center;">
					<img src="./img/logo.png" id="logo-login">
					<!-- {!! bcrypt('hola'); !!} -->
				</div>
			</div>
		</div>

		<!-- Recuperar Contraseña -->
		<div class="container" id="forgot">
			<!-- <a href="" id="text-forgot">RECUPERA TU CONTRASEÑA</a> -->
			<a class="btn btn-link" id="text-forgot" href="{{ route('password.request') }}">
                                    {{ __('RECUPERA TU CONTRASEÑA') }}
        </a>
		</div>
		<!-- <a class="btn btn-link" id="forgot" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
        </a> -->
		
	</div>
</div>
@endsection
