<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titu_principal_ing');
            $table->string('titu_principal_esp');
            $table->string('desc_principal_ing');
            $table->string('decp_principal_esp');

            $table->string('detalle_titu_1_ing');
            $table->string('detalle_titu_1_esp');
            $table->string('detalle_desc_1_ing');
            $table->string('detalle_desc_1_esp');

            $table->string('detalle_titu_2_ing');
            $table->string('detalle_titu_2_esp');
            $table->string('detalle_desc_2_ing');
            $table->string('detalle_desc_2_esp');            

            $table->string('detalle_titu_3_ing');
            $table->string('detalle_titu_3_esp');
            $table->string('detalle_desc_3_ing');
            $table->string('detalle_desc_3_esp');            

            $table->string('detalle_titu_4_ing');
            $table->string('detalle_titu_4_esp');
            $table->string('detalle_desc_4_ing');
            $table->string('detalle_desc_4_esp');            

            $table->string('img_cover_dsk');            
            $table->string('img_cover_mbl');

            $table->string('info_titu_ing');
            $table->string('info_titu_esp');            
            $table->string('info_desc_ing');
            $table->string('info_desc_esp');            

            $table->string('itin_hora_1');            
            $table->string('itin_text_1_ing');
            $table->string('itin_text_1_esp');
            $table->string('itin_hora_2');            
            $table->string('itin_text_2_ing');
            $table->string('itin_text_2_esp');
            $table->string('itin_hora_3');            
            $table->string('itin_text_3_ing');
            $table->string('itin_text_3_esp');
            $table->string('itin_hora_4');            
            $table->string('itin_text_4_ing');
            $table->string('itin_text_4_esp');

            $table->string('req_1_ing');            
            $table->string('req_1_esp');            
            $table->string('req_2_ing');            
            $table->string('req_2_esp');            
            $table->string('req_3_ing');
            $table->string('req_3_esp');            
            
            $table->string('incl_transporte');
            $table->string('incl_trekking');
            $table->string('incl_picnic');
            $table->string('incl_foto');
            $table->string('incl_polainas');

            $table->string('img_portada_dsk');
            $table->string('img_portada_mbl');

            $table->string('galeria_dsk');            
            $table->string('galeria_mbl');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
