<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_ing');
            $table->text('texto_esp');
            $table->text('src_dskt');
            $table->text('src_mbl');
            $table->text('cover_texto_ing');
            $table->text('cover_texto_esp');
            $table->text('cover_src_img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_sections');
    }
}
