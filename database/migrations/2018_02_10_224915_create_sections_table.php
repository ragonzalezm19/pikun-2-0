<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titu_1_ing');
            $table->string('titu_1_esp');
            $table->string('desc_1_ing');
            $table->string('desc_1_esp');
            $table->string('fond_1_dsk');
            $table->string('fond_1_mbl');

            $table->string('titu_2_ing');
            $table->string('titu_2_esp');
            $table->string('desc_2_ing');
            $table->string('desc_2_esp');
            $table->string('fond_2_dsk');
            $table->string('fond_2_mbl');

            $table->string('titu_3_ing');
            $table->string('titu_3_esp');
            $table->string('desc_3_ing');
            $table->string('desc_3_esp');
            $table->string('fond_3_dsk');
            $table->string('fond_3_mbl');

            $table->string('titu_4_ing');
            $table->string('titu_4_esp');
            $table->string('desc_4_ing');
            $table->string('desc_4_esp');
            $table->string('fond_4_dsk');
            $table->string('fond_4_mbl');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
