<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;

class HomeAboutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Home 
    public function index()
    {
        $sectionInfo = Section::find(1);
        // let hora = $sectionInfo->updated_at;
        // hora = hora[9];
        // $sectionInfo->updated_at = hora;
        return view("home_edit", compact('sectionInfo'));
    }

    // About Us
    public function indexAbout()
    {
        $sectionInfo = Section::find(2);
        // let hora = $sectionInfo->updated_at;
        // hora = hora[9];
        // $sectionInfo->updated_at = hora;
        return view("about", compact('sectionInfo'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // $sectionInfo = Section::find($id);
        // $sectionInfo->updated_at = $request->input('updated_at');
        // $sectionInfo->titu_1_ing = $request->input('titu_1_ing');
        // $sectionInfo->titu_1_esp = $request->input('titu_1_esp');
        // $sectionInfo->desc_1_ing = $request->textarea('desc_1_ing');
        // $sectionInfo->desc_1_esp = $request->textarea('desc_1_esp');
        // $sectionInfo->save();

        // return "Modificado con éxito";
        // $home = Section::findOrFail($id);

        // return view('home', compact('home'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // Para Home Update
    public function update(Request $request)
    {
        $sectionInfo = Section::find($request->get('id_registro'));
        $sectionInfo->titu_1_ing = $request->get('titu_1_ing');
        $sectionInfo->titu_1_esp = $request->get('titu_1_esp');
        $sectionInfo->desc_1_ing = $request->get('desc_1_ing');
        $sectionInfo->desc_1_esp = $request->get('desc_1_esp');

         $sectionInfo->save();
        return redirect('home_edit');
    }

    // Update About us
    public function updateus(Request $request)
    {

        $sectionInfo = Section::find($request->get('id_registro'));
        $sectionInfo->titu_1_ing = $request->get('titu_1_ing');
        $sectionInfo->titu_1_esp = $request->get('titu_1_esp');
        $sectionInfo->desc_1_ing = $request->get('desc_1_ing');
        $sectionInfo->desc_1_esp = $request->get('desc_1_esp');

        $sectionInfo->titu_2_ing = $request->get('titu_2_ing');
        $sectionInfo->titu_2_esp = $request->get('titu_2_esp');
        $sectionInfo->desc_2_ing = $request->get('desc_2_ing');
        $sectionInfo->desc_2_esp = $request->get('desc_2_esp');

        $sectionInfo->titu_3_ing = $request->get('titu_3_ing');
        $sectionInfo->titu_3_esp = $request->get('titu_3_esp');
        $sectionInfo->desc_3_ing = $request->get('desc_3_ing');
        $sectionInfo->desc_3_esp = $request->get('desc_3_esp');

        $sectionInfo->titu_4_ing = $request->get('titu_4_ing');
        $sectionInfo->titu_4_esp = $request->get('titu_4_esp');
        $sectionInfo->desc_4_ing = $request->get('desc_4_ing');
        $sectionInfo->desc_4_esp = $request->get('desc_4_esp');
        $sectionInfo->save();
        return redirect('about_us');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
