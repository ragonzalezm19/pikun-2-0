<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@main');

// function () {
//     return view('welcome');
Auth::routes();


// route::get('logout', 'HomeController@logut')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('home_edit', 'HomeAboutController@index')->name('home_edit');
Route::POST('home_update', 'HomeAboutController@update')->name('home_update');

Route::get('about_us', 'HomeAboutController@indexAbout')->name('about_us');
Route::POST('about_update', 'HomeAboutController@updateus')->name('about_update');